<?php

// default folder to scan
$dir = "app/code";
// default output file name
$file = "modules.md";

if (!in_array($argv[1], ['help', 'h', '?']) || !in_array($argv[2], ['help', 'h', '?'])) {
    $dir = $argv[1] ?: $dir;
    $file = $argv[2] ?: $file;
} else {
    echo "Scans given folder of Magento 2 and finds moodules and their versions.\n";
    echo "If module contains composer.json file, some additional data are parsed out of it.\n";
    echo "Markdown report is generated.\n";
    echo "\n";
    echo "Use:\n";
    echo "php magento-modules.php\t{path-to-code-directory}\t{output-file}";
    echo "\n";
    echo "Default values:\n";
    echo "path-to-code-directory:\t\tapp/code\n";
    echo "output-file:\t\t\tmodules.md\n";
    exit(0);
}

// fetch app/etc/config.php
$config = include $dir."/../"."etc/config.php";
if (!$config) {
    echo "Config not found!\n";
    exit -1;
} else {
    $config = $config['modules'];
}

echo "\nScanning: ".$dir."\n";
// basic report header
$fileContent = "# 3rd party modules scan for Magento 2\n";
$fileContent .= "## Generated: ".date("d.m.Y H:i:s")."\n";
$fileContent .= "## Path: ".$dir."\n";
$fileContent .= "Vendor|Module|Active|Version (module.xml)|Version (composer.json)|Link (if found)\n";
$fileContent .= "---|---|---|---|---|---\n";

// 1st level folder to scan, default value is "app/code"
$nodes = scandir($dir);
foreach ($nodes as $node) {
    $vendorFolder = $dir.$node;
    // 2nd level of folders, normally contains vendor name
    if (is_dir($vendorFolder) && $node !== '.' && $node !== '..') {
        echo $node."\n";
        $modules = scandir($vendorFolder);
        // 3rd level of folders, modules
        foreach ($modules as $module) {
            if (is_dir($vendorFolder.'/'.$module) && $module !== '.' && $module !== '..') {
                // test if this is really Magento 2 module
                // as each module must have registration.php to be a valid M2 module
                $moduleFolder = ($vendorFolder.'/'.$module);
                if (file_exists($moduleFolder.'/registration.php')) {
                    // test if composer.json found
                    $homepage = null;
                    if (file_exists($moduleFolder.'/composer.json')) {
                        $moduleData = json_decode(file_get_contents($moduleFolder.'/composer.json'), true);
                        // try to get module version from composer.json
                        $version = array_key_exists('version', $moduleData) ? $moduleData['version'] : null;
                        // create homepage link if any
                        $homepage = array_key_exists('homepage', $moduleData) 
                            ? "[Homepage](".$moduleData['homepage'].")" 
                            : null;
                    }

                    $moduleData = new SimpleXMLElement(file_get_contents($moduleFolder.'/etc/module.xml'));
                    $versionFromModule = $moduleData->module->attributes()->setup_version[0];

                    // print version and module name
                    echo "\t[".($version !== null && (string)$version !== (string)$versionFromModule ? $version.' x '.$versionFromModule: $versionFromModule)."] ".$module."\n";
                    // test if active or not
                    $active = (bool)$config[$node.'_'.$module] ? '<span style="color:green">Yes</span>' : '<span style="color:red">No</span>';
                    // append fetched data to report
                    $fileContent .= $node."|".$module."|".$active."|".$versionFromModule."|".($version !== null && (string)$version !== (string)$versionFromModule ? $version : null)."|".$homepage."\n";

                } else {
                    // invalid module catched
                    echo "\t[!] ".$module." from ".$node." seems not to be a valid Magento 2 module\n";
                }
            }
        }

    }
}

// generates report
file_put_contents($file, $fileContent);
