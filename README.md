# Magento 2 module scan
Simple script to scan Magento 3rd party modules.

## Usage
Run `php magento-modules.php`in Magento 2 project root. Script will go over the `app/code` folder and creates a simple `modules.md` report.

### Advanced usage

Script accepts 2 parameters:
- path to scan
- name of report file 

To scan given path use:
`php magento-modules.php /home/user/projects/magento/`

To scan given path and use different report name use:
`php magento-modules.php /home/user/projects/magento/ report.md`